function animate(obj, target, callback) {
    clearInterval(obj.timer);//首先先清除定时器，防止过多定时器的叠加

    obj.timer = setInterval(() => {
        if (obj.offsetLeft == target) {//当目标盒子的left值等于目标距离时清除定时器
            clearInterval(obj.timer);
            // if (callback) {
            //   callback();
            // }相当于：
            callback && callback();
        } else {
            obj.step = (target - obj.offsetLeft) / 10;  //这行代码可以实现变速的移动，不懂的自己代入数值计算
            if (obj.step >= 0) {
                obj.step = Math.ceil(obj.step);
            } else {
                obj.step = Math.floor(obj.step);
            }

            obj.style.left = obj.offsetLeft + obj.step + "px";
        }
    }, 15);
}
