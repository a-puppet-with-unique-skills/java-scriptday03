window.addEventListener('load',function(){
    let focus = document.querySelector('.focus');
    let ul = document.querySelector('.focus ul');
    let ulli = ul.children;
    let left = document.querySelector('.left');
    let right = document.querySelector('.right');
    let currents = document.querySelectorAll('ol li');
    let num = 0;
    let w = focus.offsetWidth;
    let ol = focus.children[1];
    let timer  =this.setInterval(function(){
        num++;
        let translatex = -num * w;
        ul.style.transition = 'all .3s';
        ul.style.transform = 'translatex('+ translatex + 'px)';
    },2000);

    //无缝滚动
    ul.addEventListener('transitionend',function(){
        if(num>=3){
             num = 0;
            ul.style.transition = 'none';
            let translatex = -num * w;
            ul.style.transform = 'translatex('+ translatex + 'px)';
        }else if(num<0){
            num=2;
            ul.style.transition = 'none';
            let translatex = -num * w;
            ul.style.transform = 'translatex('+ translatex + 'px)';
        }
        //小圆点跟着变化
        document.querySelector('.current').classList.remove('current');
        currents[num].classList.add('current');
    })
        

    //鼠标事件
    focus.addEventListener('mouseenter',function(){
        left.style.display = 'block';
        right.style.display = 'block';

    });
    focus.addEventListener('mouseleave',function(){
        left.style.display = 'none';
        right.style.display = 'none';
        
    });
    //给小圆点添加点击事件
    for(let i=0;i<currents.length;i++){
        let li =  currents[i];
        li.setAttribute('data-id',i);
        li.addEventListener('click',function(){
            for(let j = 0;j<currents.length;j++){
                currents[j].className = '';
             };
             this.className = 'current';
             num = this.dataset.id;
            //  console.log(num);

        });
       
    }

})


